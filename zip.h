#pragma once

#include <stdint.h>
#include <stddef.h>

#define ZIP_FOOTER_MAGIC  0x06054b50 // "PK\5\6"
#define ZIP_CENTRAL_MAGIC 0x02014b50 // "PK\1\2"
#define ZIP_HEADER_MAGIC  0x04034b50 // "PK\3\4"

struct zip_basics *zip_index(char *const restrict file, size_t const size, int *const restrict /* _Nullable */ error);
char *zip_error(int code); // error messages do NOT have newlines

struct zip_index {
	struct zip_central *central; // 8  8  4  4
	struct zip_header *header;   // 8 16  4  8
	void *data;                  // 8 24  4 12
	uint8_t *filename;           // 8 32  4 16
	size_t filename_length;      // 8?40  4 20
	uint32_t crc32;              // 4 44  4 24
	//uint32_t compressed_size;  // unneeded
	uint32_t original_size;      // 4 48  4 28
	uint16_t method;             // 2 50  2 30
	uint16_t flags;              // 2 52  2 32
};

struct zip_basics {
	//uint32_t metadata_offset;
	//uint32_t metadata_size;
	size_t file_count;
	//uint16_t comment_length; // padding
	//uint32_t size; // if this overflows, you have bigger issues to worry about than it not being size_t
	struct zip_index files[];
};

struct dos_date {
	uint16_t second: 5, minute: 6, hour: 5;
	uint16_t day:    5, month:  4, year: 7;
};

struct __attribute__((__packed__)) zip_footer {
	uint32_t magic;
	uint16_t disk_no;
	uint16_t central_disk_no;
	uint16_t headers_no;
	uint16_t headers_no_total;
	uint32_t headers_size;
	uint32_t headers_addr;
	uint16_t comment_length;
	uint8_t comment[];
};

struct __attribute__((__packed__)) zip_central {
	uint32_t magic;
	uint16_t ver_used;
	uint16_t ver_needed;
	uint16_t flags;
	uint16_t compression;
	struct dos_date mtime;
	uint32_t checksum;
	uint32_t compressed_size;
	uint32_t original_size;
	uint16_t filename_length;
	uint16_t extra_length;
	uint16_t comment_length;
	uint16_t file_disk_no;
	uint16_t internal_attr;
	uint32_t external_attr;
	uint32_t file_addr;
	uint8_t filename[];
};

struct __attribute__((__packed__)) zip_header {
	uint32_t magic;
	uint16_t ver_needed;
	uint16_t flags;
	uint16_t compression;
	struct dos_date mtime;
	uint32_t checksum;
	uint32_t compressed_size;
	uint32_t original_size;
	uint16_t filename_length;
	uint16_t extra_length;
	uint8_t filename[];
};
