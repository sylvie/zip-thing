#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>

#include "zip.h"

void *load_file(FILE *const restrict file, size_t *const restrict outsize);

int main(int const argc, char const *const *const argv) {
	{// check endianness and how struct bitfields are filled
		char endianness_chk[sizeof(short)] = {1};
		if (*(short *)(void *)endianness_chk != 1) {
			fputs("this software does not support big endian architechtures\n", stderr);
			exit(EXIT_FAILURE);
		}
		char struct_chk[sizeof(struct dos_date)] = {1};
		if (((struct dos_date *)(void *)struct_chk)->second != 1) {	
			fputs("this software requires bitfields to be filled in a specific way\n", stderr);
			exit(EXIT_FAILURE);
		}
	}
	
	// make sure a file is passed
	if (argc != 2) {
		fprintf(stderr, "%s: expected 2 arguments\n"
			"usage: %s <file or \"-\">\n", argv[0], argv[0]);
		exit(EXIT_FAILURE);
	}
	
	// open the file
	FILE *zip = (argv[1][0] == '-' && argv[1][1] == '\0')? stdin: fopen(argv[1], "rb");
	if (zip == NULL) {
		perror("fopen");
		exit(EXIT_FAILURE);
	}
	
	// load it into memory
	size_t size;
	char *file = load_file(zip, &size);
	if (file == NULL) {
		perror("opening file"); // "opening file: Succeeded"
		exit(EXIT_FAILURE); // "process returned with status 1" id love to see that happen
	}
	fclose(zip);
	
	// index the archive
	int error;
	struct zip_basics *headers = zip_index(file, size, &error);
	if (headers == NULL) {
		fprintf(stderr, "%s\n", zip_error(error));
		exit(EXIT_FAILURE);
	}
	
	// print some info
	if (headers->file_count)
		fprintf(stderr, "%s: zip file containing %zu file(s)\n", argv[1], headers->file_count);
	else
		fprintf(stderr, "%s: empty zip file\n", argv[1]);
	
	struct zip_index *files = headers->files;
	if (headers->file_count)
		fputs("\ncompressed - original  date ------ time  method  filename\n", stderr);
	for (uint16_t i = 0; i < headers->file_count; i++) {
		struct dos_date src_date = files[i].central->mtime;
		struct tm time = {.tm_sec = src_date.second * 2, .tm_min = src_date.minute, .tm_hour = src_date.hour, .tm_mday = src_date.day, .tm_mon = src_date.month, .tm_year = src_date.year + 80};
		char date[20];
		int length = (int) strftime(date, sizeof(date), "%Y-%m-%d %H:%M", &time);
		fprintf(stderr, "%10u/%10u  %*s  %6u  %.*s\n", files[i].central->compressed_size, files[i].central->original_size, length, date, files[i].central->compression, files[i].central->filename_length, files[i].central->filename);
		if (files[i].central->comment_length)
			fprintf(stderr, "%u byte comment: %.*s\n", files[i].central->comment_length, files[i].central->comment_length, files[i].central->filename + files[i].central->filename_length + files[i].central->extra_length);
		if (files[i].central->extra_length) {
			fprintf(stderr, "%u bytes of extra data\n", files[i].central->extra_length);
			uint16_t j = files[i].central->extra_length; unsigned char *k = files[i].central->filename + files[i].central->filename_length;
			while (j) {
				uint16_t l = k[2] + (uint16_t) (k[3] << 8);
				fprintf(stderr, "%c%c: %uB\n", k[0], k[1], l); // 7z my beloved my one true love has a '\n\0' field. lovely.
					// TODO: printing text unprintable characters escaped
				if (j < l)
					exit(EXIT_FAILURE);
				j -= l + 4;
				k += l + 4;
			}
		}
	}
	
	exit(EXIT_SUCCESS);
}

void *load_file(FILE *const restrict file, size_t *const restrict outsize) { // open and fully load a file
	if (ftell(file) == -1L) {
		if (errno != ESPIPE) {
			// perror("ftell");
			return NULL;
		}
		fputs("opening pipes isnt supported yet\n", stderr);
		return NULL;
	} else {
		if (fseek(file, 0, SEEK_END)) {
			// perror("fseek");
		}
		long ssize = ftell(file);
		size_t size = (size_t) ssize;
		if (ssize == -1L) {
			// perror("ftell");
			return NULL;
		}
		rewind(file);
		void *buffer = malloc(size);
		if (buffer == NULL) {
			// perror("malloc");
			return NULL;
		}
		if (fread(buffer, 1, size, file) != size) {
			if (ferror(file))
				fputs("ferror set\n", stderr); // the internet was VERY helpful
			// perror("fread");
			return NULL;
		}
		if (outsize != NULL)
			*outsize = size;
		return buffer;
	}
}
