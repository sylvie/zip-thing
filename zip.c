#include <stdlib.h> // malloc

#include "zip.h"

static struct zip_footer *zip_find_footer(char *const file, size_t const size, int *const error);
static int index_zip(struct zip_index *const restrict headers, char *const restrict file, struct zip_footer const *const restrict footer);

enum {
	ZIP_OK,
	ZIP_BIG,
	ZIP_SMALL,
	ZIP_SPLIT,
	ZIP_NOT_ZIP,
	ZIP_BAD_SIGNATURE,
	ZIP_UNSUPPORTED,
	ZIP_SIZE_MISMATCH,
};

char *zip_error(int code) {
	return (char *[]) {
		"ok"
		"probably not a zip file (file too big)",
		"not a zip file (file too smol)",
		"split archives arent supported",
		"found no zip footer",
		"bad signature",
		"unsupported configuration",
		"size mismatch",
	} [code];
}

struct zip_basics *zip_index(char *const restrict file, size_t const size, int *const restrict error) {
	int err;
	// find and verify the footer
	struct zip_footer *footer = zip_find_footer(file, size, &err);
	if (footer == NULL) {
		error && (*error = err);
		return NULL;
	}
	
	// The Rest &trade;
	struct zip_basics *headers = malloc(sizeof(*headers) + footer->headers_no * sizeof(*headers->files));
	err = index_zip(headers->files, file, footer);
	if (err) {
		error && (*error = err);
		free(headers);
		return NULL;
	}
	headers->file_count = footer->headers_no;
	return headers;
}

static struct zip_footer *zip_find_footer(char *const file, size_t const size, int *const error) {
	if ((size_t) 1 << 32 && size >= (size_t) 1 << 32) {
		*error = ZIP_BIG;
		return NULL;
	}

	// obligatory filesize check
	if (size < sizeof(struct zip_footer)) {
		*error = ZIP_SMALL;
		return NULL;
	}

	// scan for the footer
	for (size_t i = 0, bounds = size - sizeof(struct zip_footer) + 1; i < 65536 /* hacky limit */ && /* obligatory check */ bounds; i++, bounds--) {
		struct zip_footer *footer = (struct zip_footer *) (file + (size - sizeof(struct zip_footer) - i));
		if (
			footer->magic != ZIP_FOOTER_MAGIC               || // incorrect signature
			footer->comment_length != i                     || // incorrect comment length
			footer->headers_no_total < footer->headers_no   || // logical error
			footer->headers_addr > size                     || // metadata allegedly located after EOF
			footer->headers_addr + footer->headers_size > size // metadata ends after EOF
		) {
			continue; // not the footer / unusable, try again
		}
		if (
			footer->disk_no != footer->central_disk_no || // not the central disk
			footer->headers_no_total > footer->headers_no // file doesnt contain all metadata
		) {
			*error = ZIP_SPLIT; // could also be a genuine error..
			continue;
		}
		*error = ZIP_OK;
		return footer; // 100% valid footer, safe to return
	}
	if (*error != ZIP_SPLIT) {
		*error = ZIP_NOT_ZIP;
	}
	return NULL;
}

static int index_zip(struct zip_index *const restrict headers, char *const restrict file, struct zip_footer const *const footer) {
	// needs more error checking
	char *current_header = file + footer->headers_addr;
	for (uint16_t i = 0; i < footer->headers_no; i++) {
		struct zip_central *a = (void *) current_header;
		struct zip_header *b = (void *) (file + a->file_addr);
		if (a->magic != ZIP_CENTRAL_MAGIC) {
			return ZIP_BAD_SIGNATURE;
		}
		if (a->flags & 0x0079) { // 0b0000'0000'0111'1001, that is encryption, data trailer, "enhanced deflating", or patches
			return ZIP_UNSUPPORTED;
		}

		headers[i].central = a;
		headers[i].header = b;
		if (headers[i].header->magic != ZIP_HEADER_MAGIC) {
			return ZIP_BAD_SIGNATURE;
		}
		
		headers[i].data = headers[i].header->filename + b->filename_length + b->extra_length;
		headers[i].filename = b->filename;
		headers[i].filename_length = b->filename_length;
		headers[i].crc32 = b->checksum;
		//headers[i].compressed_size = a->compressed_size;
		headers[i].original_size = b->original_size;
		headers[i].method = b->compression;
		headers[i].flags = b->flags;
		current_header += sizeof(struct zip_central) + a->filename_length + a->extra_length + a->comment_length;
	}
	if (current_header - footer->headers_size != file + footer->headers_addr) {
		return ZIP_SIZE_MISMATCH;
	}
	return 0;
}
