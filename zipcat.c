#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <zlib.h>

#include "zip.h"

void *load_file(FILE *const restrict file, size_t *const restrict outsize);
void *inflateWrapped(void *const restrict data, uint32_t const outsize);

int main(int const argc, char const *const *const argv) {
	{// check endianness and how struct bitfields are filled
		char endianness_chk[sizeof(short)] = {1};
		if (*(short *)(void *)endianness_chk != 1) {
			fputs("this software does not support big endian architechtures\n", stderr);
			exit(EXIT_FAILURE);
		}
		char struct_chk[sizeof(struct dos_date)] = {1};
		if (((struct dos_date *)(void *)struct_chk)->second != 1) {	
			fputs("this software requires bitfields to be filled in a specific way\n", stderr);
			exit(EXIT_FAILURE);
		}
	}
	
	int status = EXIT_SUCCESS;
	bool format_hint = false;
	
	// make sure a file is passed
	if (argc != 2) {
		fprintf(stderr, "%s: expected 2 arguments\n"
			"usage: %s <file or \"-\">\n", argv[0], argv[0]);
		exit(EXIT_FAILURE);
	}
	
	// open the file
	FILE *zip = (argv[1][0] == '-' && argv[1][1] == '\0')? stdin: fopen(argv[1], "rb");
	if (zip == NULL) {
		perror("fopen");
		exit(EXIT_FAILURE);
	}
	
	// load it into memory
	size_t size;
	char *file = load_file(zip, &size);
	if (file == NULL) {
		perror("reading file"); // "opening file: Succeeded"
		exit(EXIT_FAILURE); // "process returned with status 1" id love to see that happen
	}
	fclose(zip);
	
	// index the archive
	int error;
	struct zip_basics *headers = zip_index(file, size, &error);
	if (headers == NULL) {
		fprintf(stderr, "%s\n", zip_error(error));
		exit(EXIT_FAILURE);
	}
	
	struct zip_index *files = headers->files;
	for (uint16_t i = 0; i < headers->file_count; i++) {
		char *methodname = "unknown";
		switch (files[i].method) {
			case  0: methodname = "STORE";     break;
			case  8: methodname = "DEFLATE";   break;
			case  9: methodname = "DEFLATE64"; break;
			case 12: methodname = "BZIP2";     break;
			case 14: methodname = "LZMA";      break;
			case 93: methodname = "Zstd";      break;
			case 95: methodname = "XZ";        break;
			default: break;
		}
		printf("==> %.*s (%s) <==\n", (int) files[i].filename_length, files[i].filename, methodname);
		switch (files[i].method) {
			void *data;
			case 0:
				fwrite(files[i].data, 1, files[i].original_size, stdout);
				break;
			case 8:
				data = inflateWrapped(files[i].data, files[i].original_size);
				if (data == NULL) {
					fprintf(stderr, "%s: dont know how to decompress \"%.*s\"\n", argv[0], (int) files[i].filename_length, files[i].filename);
					status = EXIT_FAILURE;
				} else {
					fwrite(data, 1, files[i].original_size, stdout);
				}
				break;
			default:
				fprintf(stderr, "%s: dont know how to decompress method %s\n", argv[0], methodname);
				status = EXIT_FAILURE;
				format_hint = true;
		}
	}
	if (format_hint) {
		fprintf(stderr, "%s: supported formats: STORE, DEFLATE\n", argv[0]);
	}
	exit(status);
}

void *load_file(FILE *const restrict file, size_t *const restrict outsize) { // open and fully load a file
	if (ftell(file) == -1L) {
		if (errno != ESPIPE) {
			// perror("ftell");
			return NULL;
		}
		fputs("opening pipes isnt supported yet\n", stderr);
		return NULL;
	} else {
		if (fseek(file, 0, SEEK_END)) {
			// perror("fseek");
		}
		long ssize = ftell(file);
		size_t size = (size_t) ssize;
		if (ssize == -1L) {
			// perror("ftell");
			return NULL;
		}
		rewind(file);
		void *buffer = malloc(size);
		if (buffer == NULL) {
			// perror("malloc");
			return NULL;
		}
		if (fread(buffer, 1, size, file) != size) {
			if (ferror(file)) {
				fputs("ferror set\n", stderr); // the internet was VERY helpful
			}
			// perror("fread");
			return NULL;
		}
		if (outsize != NULL) {
			*outsize = size;
		}
		return buffer;
	}
}

void *inflateWrapped(void *const restrict data, uint32_t const outsize) {
	z_stream stream = {
		.zalloc = Z_NULL,
		.zfree = Z_NULL,
		.opaque = Z_NULL,
	}; // zlib docs require you to set these 3 to Z_NULL, apparently
	stream.avail_in = outsize; // hope itll work! hehe
	stream.next_in = data;
	int ret = inflateInit2(&stream, -15); // -15 is raw DEFLATE, we dont want zlib + DEFLATE :p
	if (ret != Z_OK) {
		return NULL;
	}
	unsigned char *buf = malloc(outsize);
	if (buf == NULL) {
		perror("malloc");
		goto finally; // would it hurt to change it to error? free(NULL) is defined to be a no-op
	}
	stream.avail_out = outsize;
	stream.next_out = buf;
	
	switch ((ret = inflate(&stream, Z_SYNC_FLUSH))) {
		case Z_NEED_DICT:
		case Z_DATA_ERROR:
			//fputs("inflate: data error\n", stderr);
			goto error;
		case Z_MEM_ERROR:
			//fputs("inflate: memory error\n", stderr);
			goto error;
		case Z_STREAM_ERROR:
			//fputs("inflate: stream error\n", stderr);
			goto error;
		case Z_OK:
			//fputs("inflate: stream didnt end", stderr);
			goto error;
		
		case Z_STREAM_END:
			goto finally;
		
		default:
			//fprintf(stderr, "unrecognised return value %u\n", ret);
			goto error;
	}
	
	error: // all cases share the error handling code
	free(buf);
	buf = NULL; // return value
	
	finally:
	(void) inflateEnd(&stream);
	//fprintf(stderr, "processed %lu (0x%lx) bytes total, exit status %d\n", stream.total_out, stream.total_out, ret);
	return buf;
}
